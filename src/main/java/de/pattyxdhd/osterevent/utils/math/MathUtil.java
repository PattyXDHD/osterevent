package de.pattyxdhd.osterevent.utils.math;

import java.util.Random;

public class MathUtil {

    public static int randomInt(int start, int stop) {
        Random random = new Random();
        int randomInt = random.nextInt(stop);
        while (randomInt < start) {
            randomInt = random.nextInt(stop);
        }
        return randomInt;
    }

}
