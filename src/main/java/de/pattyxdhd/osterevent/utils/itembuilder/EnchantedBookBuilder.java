package de.pattyxdhd.osterevent.utils.itembuilder;

import com.google.common.collect.Lists;
import com.google.common.collect.Maps;
import org.bukkit.Material;
import org.bukkit.enchantments.Enchantment;
import org.bukkit.inventory.ItemFlag;
import org.bukkit.inventory.ItemStack;
import org.bukkit.inventory.meta.EnchantmentStorageMeta;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;

public class EnchantedBookBuilder {

    private final Material material;
    private int amount;
    private final short durability;
    private boolean unbreakable;
    private String displayName;
    private List<String> lore;
    private final Map<Enchantment, Integer> enchantmentIntegerMap;
    private final ArrayList<ItemFlag> itemFlags;

    public EnchantedBookBuilder() {
        this.material = Material.ENCHANTED_BOOK;
        this.amount = 1;
        this.durability = 0;
        this.unbreakable = false;
        this.enchantmentIntegerMap = Maps.newConcurrentMap();
        this.itemFlags = Lists.newArrayList();
    }

    public ItemStack build() {
        ItemStack itemStack = new ItemStack(this.material, this.amount, this.durability);
        EnchantmentStorageMeta itemMeta = (EnchantmentStorageMeta) itemStack.getItemMeta();
        if (this.unbreakable) {
            itemMeta.spigot().setUnbreakable(true);
        }

        if (this.displayName != null) {
            itemMeta.setDisplayName(this.displayName);
        }

        if (this.lore != null) {
            itemMeta.setLore(this.lore);
        }

        if (this.itemFlags != null) {
            itemFlags.forEach(itemMeta::addItemFlags);
        }

        if (!enchantmentIntegerMap.isEmpty()) {
            this.enchantmentIntegerMap.forEach((enchantment, integer) -> {
                itemMeta.addStoredEnchant(enchantment, integer, true);
            });
        }

        itemStack.setItemMeta(itemMeta);
        return itemStack;
    }

    public EnchantedBookBuilder setAmount(int amount) {
        this.amount = amount;
        return this;
    }

    public EnchantedBookBuilder setDisplayName(String displayName) {
        this.displayName = displayName;
        return this;
    }

    public EnchantedBookBuilder setLore(List<String> lore) {
        this.lore = lore;
        return this;
    }

    public EnchantedBookBuilder addEnchantment(Enchantment enchantment, int level) {
        this.enchantmentIntegerMap.put(enchantment, level);
        return this;
    }

    public EnchantedBookBuilder addItemFlag(ItemFlag itemFlag) {
        this.itemFlags.add(itemFlag);
        return this;
    }

    public EnchantedBookBuilder setUnbreakable(boolean unbreakable) {
        this.unbreakable = unbreakable;
        return this;
    }

}
