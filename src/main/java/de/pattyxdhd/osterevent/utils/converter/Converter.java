package de.pattyxdhd.osterevent.utils.converter;

import com.google.common.collect.Lists;
import org.bukkit.Bukkit;
import org.bukkit.Location;
import org.bukkit.World;

import java.util.List;

public class Converter {

    public Converter() {

    }

    public String convertLocationToString(final Location location) {
        return location.getX() + ";" +
                location.getY() + ";" +
                location.getZ() + ";" +
                location.getYaw() + ";" +
                location.getPitch() + ";" +
                location.getWorld().getName();
    }

    public Location convertStringToLocation(final String locationValue) {
        String[] args = locationValue.split(";");
        World world = null;
        try {
            world = Bukkit.getWorld(args[5]);
        } catch (ArrayIndexOutOfBoundsException ignored) {
        }

        if (world == null) {
            return null;
        }

        return new Location(world, toDouble(args[0]), toDouble(args[1]), toDouble(args[2]), toFloat(args[3]), toFloat(args[4]));
    }

    public List<String> convertFromLocationListToStringlist(final List<Location> locationList) {
        List<String> stringList = Lists.newArrayList();
        locationList.forEach(location -> stringList.add(convertLocationToString(location)));
        return stringList;
    }

    public List<Location> convertFromStringListToLocationlist(final List<String> stringList) {
        List<Location> locationList = Lists.newArrayList();
        stringList.forEach(string -> locationList.add(convertStringToLocation(string)));
        return locationList;
    }

    public List<String> translateColorCodes(final List<String> strings) {
        final List<String> newStrings = Lists.newArrayList();
        strings.forEach(s -> newStrings.add(s.replace("&", "§")));
        return newStrings;
    }

    public List<String> replace(final List<String> strings, final String befor, final String after) {
        final List<String> newStrings = Lists.newArrayList();
        strings.forEach(s -> newStrings.add(s.replace(befor, after)));
        return newStrings;
    }

    private Double toDouble(final String d) {
        try {
            return Double.valueOf(d);
        } catch (Exception e) {
            return 0D;
        }
    }

    private Float toFloat(final String f) {
        try {
            return Float.valueOf(f);
        } catch (Exception e) {
            return 0F;
        }
    }

}
