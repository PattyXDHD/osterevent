package de.pattyxdhd.osterevent.utils.objects;

import com.google.common.collect.Lists;
import de.pattyxdhd.osterevent.OsterEvent;
import de.pattyxdhd.osterevent.utils.uuidfetcher.UUIDFetcher;
import org.bukkit.Bukkit;
import org.bukkit.Location;
import org.bukkit.entity.Player;

import java.util.List;
import java.util.UUID;

public class EasterPlayer {

    private final String uuid;
    private final List<Location> foundLocations;
    private final List<Hologram> holograms;

    public EasterPlayer(String uuid, List<Location> foundLocations) {
        this.uuid = uuid;
        this.foundLocations = foundLocations;
        this.holograms = Lists.newArrayList();
    }

    public String getUuid() {
        return uuid;
    }

    public Integer getFoundCounter() {
        if (foundLocations.isEmpty()) {
            return 0;
        }
        return foundLocations.size();
    }

    public void loadHolograms() {
        foundLocations.forEach(location -> {
            final Location editLocation = new Location(location.getWorld(), location.getX() + 0.5, location.getY() + 0.6, location.getZ() + 0.5);

            Hologram hologram = new Hologram(
                    OsterEvent.getInstance().getConverter().replace(OsterEvent.getInstance().getFileManager().getHoloText(), "%playerName%", UUIDFetcher.getName(UUID.fromString(this.uuid))),
                    editLocation);
            this.holograms.add(hologram);
        });
    }

    public void showHolograms() {
        holograms.forEach(hologram -> {
            if (getPlayer() != null) {
                hologram.showForPlayer(getPlayer());
            }
        });
    }

    public void hideHolograms() {
        holograms.forEach(hologram -> {
            if (getPlayer() != null) {
                hologram.hideForPlayer(getPlayer());
            }
        });
    }

    public void addAndShowHologram(final Player player, final Location location) {
        final Location editLocation = new Location(location.getWorld(), location.getX() + 0.5, location.getY() + 0.6, location.getZ() + 0.5);

        Hologram hologram = new Hologram(
                OsterEvent.getInstance().getConverter().replace(OsterEvent.getInstance().getFileManager().getHoloText(), "%playerName%", UUIDFetcher.getName(UUID.fromString(this.uuid))),
                editLocation);
        this.holograms.add(hologram);
        hologram.showForPlayer(player);
    }

    public Player getPlayer() {
        return Bukkit.getPlayer(UUID.fromString(this.uuid));
    }

    public List<Location> getFoundLocations() {
        return foundLocations;
    }

    public List<Hologram> getHolograms() {
        return holograms;
    }
}
