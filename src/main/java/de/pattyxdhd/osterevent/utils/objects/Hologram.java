package de.pattyxdhd.osterevent.utils.objects;

import com.google.common.collect.Lists;
import lombok.Getter;
import net.minecraft.server.v1_8_R3.EntityArmorStand;
import net.minecraft.server.v1_8_R3.PacketPlayOutEntityDestroy;
import net.minecraft.server.v1_8_R3.PacketPlayOutSpawnEntityLiving;
import org.bukkit.Location;
import org.bukkit.craftbukkit.v1_8_R3.CraftWorld;
import org.bukkit.craftbukkit.v1_8_R3.entity.CraftPlayer;
import org.bukkit.entity.Player;

import java.util.List;

@Getter
public class Hologram {

    private final List<EntityArmorStand> armorStands = Lists.newArrayList();
    private final List<String> text;
    private final Location location;
    private final Double distance;
    private int count;

    public Hologram(List<String> text, Location location) {
        this.text = text;
        this.location = location;
        this.distance = 0.3D;
        this.count = 0;
        createHolo();
    }

    private void createHolo() {
        this.text.forEach(s -> {
            EntityArmorStand entityArmorStand = new EntityArmorStand(((CraftWorld) this.location.getWorld()).getHandle(), this.location.getX(), this.location.getY(), this.location.getZ());
            entityArmorStand.setCustomName(s);
            entityArmorStand.setCustomNameVisible(true);
            entityArmorStand.setInvisible(true);
            entityArmorStand.setGravity(false);
            entityArmorStand.setSmall(true);
            entityArmorStand.setBasePlate(false);
            this.armorStands.add(entityArmorStand);
            this.location.subtract(0, this.distance, 0);
            count++;
        });
        for (int i = 0; i < count; i++) {
            this.location.add(0, this.distance, 0);
        }
        this.count = 0;
    }

    public void showForPlayer(final Player player) {
        this.armorStands.forEach(entityArmorStand -> {
            PacketPlayOutSpawnEntityLiving packetPlayOutSpawnEntityLiving = new PacketPlayOutSpawnEntityLiving(entityArmorStand);
            ((CraftPlayer) player).getHandle().playerConnection.sendPacket(packetPlayOutSpawnEntityLiving);
        });
    }

    public void hideForPlayer(final Player player) {
        this.armorStands.forEach(entityArmorStand -> {
            PacketPlayOutEntityDestroy packetPlayOutSpawnEntityLiving = new PacketPlayOutEntityDestroy(entityArmorStand.getId());
            ((CraftPlayer) player).getHandle().playerConnection.sendPacket(packetPlayOutSpawnEntityLiving);
        });
    }

}
