package de.pattyxdhd.osterevent.commands;

import com.google.common.collect.Lists;
import de.pattyxdhd.osterevent.OsterEvent;
import de.pattyxdhd.osterevent.utils.file.FileManager;
import de.pattyxdhd.osterevent.utils.itemcache.ItemCache;
import de.pattyxdhd.osterevent.utils.mysql.PreparedStatementBuilder;
import de.pattyxdhd.osterevent.utils.uuidfetcher.UUIDFetcher;
import lombok.Getter;
import org.bukkit.Bukkit;
import org.bukkit.command.Command;
import org.bukkit.command.CommandExecutor;
import org.bukkit.command.CommandSender;
import org.bukkit.command.TabExecutor;
import org.bukkit.entity.Player;

import java.util.List;
import java.util.UUID;

public class EasterCommand implements CommandExecutor, TabExecutor {

    @Getter
    private final FileManager fileManager;

    private final List<String> subCommands = Lists.newArrayList("getegg", "build", "help", "reload", "reset", "cleardatabase");

    public EasterCommand(FileManager fileManager) {
        this.fileManager = fileManager;
    }

    @Override
    public boolean onCommand(CommandSender sender, Command command, String label, String[] args) {

        if (!sender.hasPermission("easter.admin")) {
            sender.sendMessage(getFileManager().getPrefix() + "§eOsterEvent-System by §bPattyXDHD");
            sender.sendMessage(getFileManager().getPrefix() + "§7Authors: §9" + asStringList(OsterEvent.getInstance().getDescription().getAuthors()));
            sender.sendMessage(getFileManager().getPrefix() + "§7Version: §a" + OsterEvent.getInstance().getDescription().getVersion());
            return false;
        }

        if (!(sender instanceof Player)) {
            sender.sendMessage(getFileManager().getPrefix() + "Du musst ein Spieler sein.");
            return false;
        }

        final Player player = ((Player) sender);


        if (args.length == 1) {

            final String commandValue = args[0].toLowerCase();

            switch (commandValue) {

                case "getegg":
                    player.getInventory().addItem(ItemCache.getPlaceEgg());
                    player.sendMessage(getFileManager().getPrefix() + "§aDu hast das Setzt-Ei bekommen.");
                    if (!OsterEvent.getInstance().getEggManager().canBuild(player)) {
                        OsterEvent.getInstance().getEggManager().toggleBuild(player);
                    }
                    break;

                case "build":
                    OsterEvent.getInstance().getEggManager().toggleBuild(player);
                    break;

                case "reload":
                    OsterEvent.getInstance().getFileManager().reloadFile(OsterEvent.getInstance());
                    player.sendMessage(getFileManager().getPrefix() + "§aDie config.yml wurde neu eingelesen.");
                    break;

                case "cleardatabase":
                    OsterEvent.getInstance().getDatabaseHandler().getPlayers().forEach((s, easterPlayer) -> easterPlayer.hideHolograms());

                    OsterEvent.getInstance().getDatabaseHandler().getPlayers().clear();
                    OsterEvent.getInstance().getDatabaseHandler().getMySQL().update(new PreparedStatementBuilder("TRUNCATE TABLE EasterPlayers", OsterEvent.getInstance().getDatabaseHandler().getMySQL()).build());
                    player.sendMessage(getFileManager().getPrefix() + "§aDu hast die Datenbank geleert.");

                    new Thread(() -> {
                        Bukkit.getOnlinePlayers().forEach(target -> {
                            if (!OsterEvent.getInstance().getDatabaseHandler().existsPlayer(target.getUniqueId().toString())) {
                                OsterEvent.getInstance().getDatabaseHandler().insertPlayer(target);
                                OsterEvent.getInstance().log("§aAdded §e" + target.getName() + " §ato Database.");
                                OsterEvent.getInstance().getDatabaseHandler().getPlayers().get(target.getUniqueId().toString()).loadHolograms();
                            }
                            if (OsterEvent.getInstance().getFileManager().isHoloUse()) {
                                OsterEvent.getInstance().getDatabaseHandler().getPlayers().get(target.getUniqueId().toString()).showHolograms();
                            }
                        });
                        Thread.currentThread().stop();
                    }).start();

                    player.sendMessage(getFileManager().getPrefix() + "§eAlle Onlinespieler wurde erneut zur Datenbank hinzugefügt.");
                    break;

                default:
                    sendHelpMessage(sender);
                    break;
            }
            return false;
        }

        if (args.length == 2) {

            final String commandValue = args[0].toLowerCase();
            String value = args[1];

            switch (commandValue) {

                case "reset":
                    final UUID uuid = UUIDFetcher.getUUID(value);
                    if (uuid != null) {
                        if (OsterEvent.getInstance().getDatabaseHandler().existsPlayer(uuid.toString())) {
                            OsterEvent.getInstance().getDatabaseHandler().deletePlayer(uuid.toString());
                            player.sendMessage(getFileManager().getPrefix() + "§aDu hast §e" + value + " §aaus der Datenbank gelöscht.");
                        } else {
                            player.sendMessage(getFileManager().getPrefix() + "§cDer angegebene Spieler ist nicht in der Datenbank vorhanen.");
                        }
                    } else {
                        player.sendMessage(getFileManager().getPrefix() + "§cDer angegebene Spieler existiert nicht.");
                    }
                    break;

                default:
                    sendHelpMessage(sender);
                    break;

            }
            return false;
        }

        sender.sendMessage(getFileManager().getPrefix() + "§eOsterEvent-System by §bPattyXDHD");
        sender.sendMessage(getFileManager().getPrefix() + "§7Authors: §9" + asStringList(OsterEvent.getInstance().getDescription().getAuthors()));
        sender.sendMessage(getFileManager().getPrefix() + "§7Version: §a" + OsterEvent.getInstance().getDescription().getVersion());
        sender.sendMessage(getFileManager().getPrefix() + "§dUm hilfe zu bekommen benutze /easter help");
        return false;
    }

    @Override
    public List<String> onTabComplete(CommandSender sender, Command command, String alias, String[] args) {
        if (!sender.hasPermission("easter.admin")) {
            return null;
        }
        if (args.length == 1) {
            return getSubCommandMatches(args);
        }
        if (args.length == 2) {
            if (args[0].equalsIgnoreCase("reset")) {
                return getPlayerMatches(args);
            }
            return null;
        }
        return null;
    }

    private List<String> getSubCommandMatches(String[] args) {
        List<String> matches = Lists.newArrayList();
        if (args.length == 1) {
            String search = args[0].toLowerCase();
            for (String strings : subCommands) {
                if (strings.startsWith(search)) {
                    matches.add(strings);
                }
            }
        }
        return matches;
    }

    private List<String> getPlayerMatches(String[] args) {
        List<String> matches = Lists.newArrayList();
        if (args.length == 2) {
            String search = args[1].toLowerCase();
            for (Player player : Bukkit.getOnlinePlayers()) {
                if (player.getName().toLowerCase().startsWith(search.toLowerCase())) {
                    matches.add(player.getName());
                }
            }
        }
        return matches;
    }

    private void sendHelpMessage(final CommandSender sender) {
        sender.sendMessage(getFileManager().getPrefix() + "§6Hilfe");
        sender.sendMessage("");
        sender.sendMessage(getFileManager().getPrefix() + "/easter help");
        sender.sendMessage(getFileManager().getPrefix() + "/easter reload");
        sender.sendMessage(getFileManager().getPrefix() + "/easter getEgg");
        sender.sendMessage(getFileManager().getPrefix() + "/easter build");
        sender.sendMessage(getFileManager().getPrefix() + "/easter reset <Name>");
    }

    private String asStringList(final List<String> strings) {
        StringBuilder stringBuilder = new StringBuilder();
        strings.forEach(s -> {
            stringBuilder.append(s).append(", ");
        });
        return stringBuilder.substring(0, stringBuilder.toString().length() - 2);
    }

}
