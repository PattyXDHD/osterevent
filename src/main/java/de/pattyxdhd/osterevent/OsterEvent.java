package de.pattyxdhd.osterevent;

import de.pattyxdhd.osterevent.commands.EasterCommand;
import de.pattyxdhd.osterevent.listener.BlockBreakListener;
import de.pattyxdhd.osterevent.listener.BlockPlaceListener;
import de.pattyxdhd.osterevent.listener.InteractListener;
import de.pattyxdhd.osterevent.listener.JoinListener;
import de.pattyxdhd.osterevent.manager.EggManager;
import de.pattyxdhd.osterevent.utils.converter.Converter;
import de.pattyxdhd.osterevent.utils.file.FileManager;
import de.pattyxdhd.osterevent.utils.mysql.DatabaseHandler;
import de.pattyxdhd.osterevent.utils.mysql.MySQL;
import de.pattyxdhd.osterevent.utils.mysql.PreparedStatementBuilder;
import lombok.Getter;
import org.bukkit.Bukkit;
import org.bukkit.plugin.PluginManager;
import org.bukkit.plugin.java.JavaPlugin;

/*
        Project by PattyXDHD
 */

public class OsterEvent extends JavaPlugin {

    @Getter
    private static OsterEvent instance;

    @Getter
    private final Converter converter = new Converter();

    @Getter
    private FileManager fileManager;

    @Getter
    private DatabaseHandler databaseHandler;

    @Getter
    private EggManager eggManager;

    @Override
    public void onEnable() {
        instance = this;
        fileManager = new FileManager(getInstance());
        eggManager = new EggManager(getFileManager());

        loadMySQL();

        loadCommands();
        loadListener(Bukkit.getPluginManager());

        log("§aPlugin geladen.");
    }

    @Override
    public void onDisable() {
        log("§cPlugin entladen.");
    }

    private void loadCommands() {
        getCommand("easter").setExecutor(new EasterCommand(getFileManager()));
    }

    private void loadListener(final PluginManager pluginManager) {
        pluginManager.registerEvents(new JoinListener(), this);
        pluginManager.registerEvents(new BlockPlaceListener(), this);
        pluginManager.registerEvents(new BlockBreakListener(), this);
        pluginManager.registerEvents(new InteractListener(getFileManager()), this);
    }

    private void loadMySQL() {
        MySQL mySQL = new MySQL(getFileManager().getHost(), getFileManager().getPort(), getFileManager().getDatabase(), getFileManager().getUser(), getFileManager().getPassword(), getFileManager().getPrefix());
        mySQL.connect();
        mySQL.update(new PreparedStatementBuilder("CREATE TABLE IF NOT EXISTS EasterPlayers(uuid VARCHAR(64), foundEggsLocation VARCHAR(500))", mySQL).build());
        databaseHandler = new DatabaseHandler(mySQL);
        new Thread(() -> {
            databaseHandler.loadPlayers();
            databaseHandler.getPlayers().forEach((s, easterPlayer) -> easterPlayer.loadHolograms());
            Thread.currentThread().stop();
        }).start();
    }

    public void log(final String message) {
        Bukkit.getConsoleSender().sendMessage(fileManager.getPrefix() + message);
    }

}
