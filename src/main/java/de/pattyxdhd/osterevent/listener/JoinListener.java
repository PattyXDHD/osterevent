package de.pattyxdhd.osterevent.listener;

import de.pattyxdhd.osterevent.OsterEvent;
import org.bukkit.event.EventHandler;
import org.bukkit.event.Listener;
import org.bukkit.event.player.PlayerJoinEvent;

public class JoinListener implements Listener {

    @EventHandler
    public void onJoin(final PlayerJoinEvent event) {
        if (!OsterEvent.getInstance().getDatabaseHandler().existsPlayer(event.getPlayer().getUniqueId().toString())) {
            OsterEvent.getInstance().getDatabaseHandler().insertPlayer(event.getPlayer());
            OsterEvent.getInstance().log("§aAdded §e" + event.getPlayer().getName() + " §ato Database.");
        }

        if (OsterEvent.getInstance().getFileManager().isHoloUse()) {
            OsterEvent.getInstance().getDatabaseHandler().getPlayers().get(event.getPlayer().getUniqueId().toString()).showHolograms();
        }
    }

}
