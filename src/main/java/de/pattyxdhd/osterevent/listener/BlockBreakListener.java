package de.pattyxdhd.osterevent.listener;

import de.pattyxdhd.osterevent.OsterEvent;
import org.bukkit.Sound;
import org.bukkit.event.EventHandler;
import org.bukkit.event.Listener;
import org.bukkit.event.block.BlockBreakEvent;

public class BlockBreakListener implements Listener {

    @EventHandler
    public void onBreak(final BlockBreakEvent event) {
        if (OsterEvent.getInstance().getEggManager().exists(event.getBlock().getLocation())) {
            if (event.getPlayer().hasPermission("easter.admin")) {
                if (OsterEvent.getInstance().getEggManager().canBuild(event.getPlayer())) {
                    OsterEvent.getInstance().getEggManager().removeEgg(event.getBlock().getLocation());
                    OsterEvent.getInstance().getDatabaseHandler().getPlayers().forEach((s, easterPlayer) -> {
                        easterPlayer.getHolograms().forEach(hologram -> {

                            if (hologram.getLocation().distance(event.getBlock().getLocation()) <= 1.3) {
                                if (easterPlayer.getPlayer() != null) {
                                    hologram.hideForPlayer(easterPlayer.getPlayer());
                                }
                            }

                        });

                    });
                    event.getPlayer().sendMessage(OsterEvent.getInstance().getFileManager().getPrefix() + "§cDu hast erfolgreich ein Ei entfernt. §8(§b" + OsterEvent.getInstance().getEggManager().getEggCount() + " Eier§8)");
                    if (OsterEvent.getInstance().getFileManager().isUseSounds())
                        event.getPlayer().playSound(event.getPlayer().getLocation(), Sound.LEVEL_UP, 1, 2);
                } else {
                    event.setCancelled(true);
                    event.getPlayer().sendMessage(OsterEvent.getInstance().getFileManager().getPrefix() + "§cDu musst den Abbaumode aktivieren.");
                    if (OsterEvent.getInstance().getFileManager().isUseSounds())
                        event.getPlayer().playSound(event.getPlayer().getLocation(), Sound.ANVIL_BREAK, 1, 2);
                }
            } else {
                event.setCancelled(true);
                event.getPlayer().sendMessage(OsterEvent.getInstance().getFileManager().getPrefix() + "§cDu darfst die Ostereier doch nicht abbauen!");
                if (OsterEvent.getInstance().getFileManager().isUseSounds())
                    event.getPlayer().playSound(event.getPlayer().getLocation(), Sound.BAT_DEATH, 1, 2);
            }
        }
    }

}
